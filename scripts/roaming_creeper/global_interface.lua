local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local destinations = require("scripts.roaming_creeper.destinations")

local creeperLastSales = {}

local help =
    [[
    creeperCell = Show creeper's current cell
    creeperDest = List of all creeper's destinations
    creeperSales = List of all creeper's current wares for sale

    e.g.
    I.RoamingCreeper_eqnx.creeperSales
]]

local function getCreeper()
    return world.getObjectByFormId(core.getFormId("Morrowind.esm", 0x2af25))
end

return {
    interfaceName = "RoamingCreeper_eqnx",
    interface = setmetatable(
        {
            creeperLastSales = creeperLastSales
        },
        {
            __index = function(tbl, key)
                if key == "info" then
                    return tostring(require("scripts.roaming_creeper.modInfo"))
                end
                if key == "help" then
                    return help
                end
                if key == "creeperCell" then
                    return getCreeper().cell
                end
                if key == "creeperDest" then
                    local cell = ""
                    for _, T in pairs(destinations) do
                        cell = cell .. T.cell .. "\n"
                    end
                    return cell
                end
                if key == "creeperSales" then
                    local itms = ""
                    local fmt = "%s [%s]"
                    local inv = types.Actor.inventory(getCreeper()):getAll()
                    table.sort(creeperLastSales)
                    for _, S in pairs(creeperLastSales) do
                        for _, T in pairs(inv) do
                            if T.recordId == S then
                                itms = itms .. fmt:format(T.type.record(T).name, T.count) .. "\n"
                            end
                        end
                    end
                    return itms
                end
                if key == "nextDest" then
                    return function(bool) core.sendGlobalEvent("RoamingCreeper_nextDest_eqnx", bool) end
                end
            end
        }
    )
}
