require("scripts.roaming_creeper.openmw")
local modInfo = require("scripts.roaming_creeper.modInfo")
local globalSetting = storage.globalSection("SettingsRoamingCreeperMain")

local items = require("scripts.roaming_creeper.items")
local destinations = require("scripts.roaming_creeper.destinations")

local currentDay = math.huge -- world.mwscript.getGlobalVariables().day

local creeper = world.getObjectByFormId(core.getFormId("Morrowind.esm", 0x2af25)) -- 175909 --doesnt work if the object hasnt loaded in memory
local creeperOriginCell = "caldera, ghorak manor" --morrowind default
local hasFollowAi = false

local creeperSales = {}
local creeperLastSales = {}

local function initializeCreeper()
    -- searches the creeper if it hasnt loaded in memory yet. using linear search for now. one time operation.
    if not creeper or not creeper:isValid() or creeper.recordId ~= "scamp_creeper" then
        for _, cell in pairs(world.cells) do
            for __, b in pairs(cell:getAll(types.Creature)) do
                if b.type.record(b).id == "scamp_creeper" and util.bitAnd(b.id, 0x1FFFFF) == 0x2af25 then
                    creeper = b
                    creeperOriginCell = creeper.cell --wherever creeper may be at first, we consider this the origin(starting) cell
                    creeper:addScript("scripts/roaming_creeper/creeper.lua")
					return true
                end
            end
        end
        error("Creeper not found! Roaming Creeper will not work")
    end
end

local function initializeCreeperSales()
    for _, stuff in pairs(items) do
        for _, item in pairs(stuff) do
            table.insert(creeperSales, item)
        end
    end
    for _, book in pairs(types.Book.records) do
        if book.isScroll and book.enchant:len() > 0 then
            table.insert(creeperSales, book.id)
        end
    end
end

local function setup()
	initializeCreeper()
	initializeCreeperSales()
end

local function removeLastSales()
    for _, item in pairs(creeperLastSales) do
        local inv = types.Actor.inventory(creeper)
        local itemToRemove = inv:find(item)
        if itemToRemove then
            itemToRemove:remove()
        end
    end
    creeperLastSales = {}
    I.RoamingCreeper_eqnx.creeperLastSales = {}
end

local function addNewSales()
    for _, item in pairs(creeperSales) do
        if math.random(1000) < 100 then
            world.createObject(item, math.random(4)):moveInto(creeper)
            table.insert(creeperLastSales, item)
        end
    end
    for i, v in ipairs(creeperLastSales) do
        table.insert(I.RoamingCreeper_eqnx.creeperLastSales, v)
    end
    --future: add items from other mods
end

local function nextDest(debug)
    local dest = destinations[math.random(#destinations)]
    local position, cell = util.vector3(table.unpack(dest.position)), dest.cell
    local creeperIsDead = types.Actor.isDead(creeper)

    if hasFollowAi or not creeper or not position or not cell or creeperIsDead or not creeper:isValid() then
        return
    end

    core.vfx.spawn("VFX_Summon_end", creeper.position)

    if currentDay == 1 then -- at day 1 of the month, return to original spot (usually ghorak manor)
        position = creeper.startingPosition
        cell = creeperOriginCell
    end

    creeper:teleport(cell, position)

    pcall(removeLastSales)
    pcall(addNewSales)

    creeper:sendEvent(
        "RoamingCreeper_update_eqnx",
        {
            position = position
        }
    )

    if debug then
        for _, player in pairs(world.players) do
            player:teleport(cell, position)
            player:sendEvent("RoamingCreeper_debug_eqnx", cell)
        end
        return
    end
end

async:newUnsavableSimulationTimer(0, setup)

--future, make unique interaction/dialogue with creeper requesting specific items to be sold next visit at the cost of double the items payment
I.Activation.addHandlerForObject(
    creeper,
    function(activatedObject, actor)
        if activatedObject == creeper and actor.type == types.Player then
            actor:sendEvent(
                "RoamingCreeper_creeperActivated_eqnx",
                {
                    creeper = creeper
                }
            )
            return true --false
        end
    end
)

return {
    engineHandlers = {
        onLoad = function(data)
            if data then
                creeper = data.creeper
            end
        end,
        onSave = function()
            return {
                creeper = creeper
            }
        end,
        onUpdate = function()
            assert(
                modInfo.MIN_API <= core.API_REVISION,
                string.format("[%s] mod requires Lua API %s or newer!", modInfo.MOD_NAME, modInfo.MIN_API)
            )

            if globalSetting:get("Mod Status") and currentDay ~= world.mwscript.getGlobalVariables().day then
                currentDay = world.mwscript.getGlobalVariables().day
                pcall(nextDest)
            end
        end
    },
    eventHandlers = {
        RoamingCreeper_nextDest_eqnx = nextDest,
        -- from settings_G.lua
        RoamingCreeper_update_eqnx = function(data)
            if data then
                hasFollowAi = data.hasFollowAi or hasFollowAi
                if data.turnedOff then
                    pcall(
                        function()
                            creeper:teleport(creeperOriginCell, creeper.startingPosition, creeper.startingRotation)
                        end
                    )
                    pcall(removeLastSales)
                end
            end
        end
    }
}
