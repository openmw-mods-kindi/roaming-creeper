local I = require("openmw.interfaces")
local UI = require('openmw.interfaces').UI
local core = require("openmw.core")
local modInfo = require("scripts.roaming_creeper.modInfo")
local l10n = core.l10n("roaming_creeper")

I.Settings.registerPage {
    key = 'roaming_creeper',
    l10n = 'roaming_creeper',
    name = 'settings_modName',
    description = l10n('settings_modDesc'):format(modInfo.MOD_VERSION)
}



return {
    eventHandlers = {
        RoamingCreeper_creeperActivated_eqnx = function(data)
            local creeper = data.creeper
            --UI.addMode("Dialogue", {target = creeper})
        end,
        RoamingCreeper_debug_eqnx = function(cell)
            require("openmw.ui").showMessage("[Debug Roaming Creeper] " .. cell)
        end
    }
}
