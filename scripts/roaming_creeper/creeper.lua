require("scripts.roaming_creeper.openmw")
local AI = I.AI

local function hasFollowAi()
    local isFollowing = false
    AI.forEachPackage(function(package)
        if package.type == "Follow" or package.type == "Escort" then
            isFollowing = true
            return
        end
    end)
    return isFollowing
end

return {
    engineHandlers = {
        onUpdate = function()
            core.sendGlobalEvent("RoamingCreeper_update_eqnx", {
                hasFollowAi = hasFollowAi()
            })
        end
    },
    eventHandlers = {
        -- from global.lua
        RoamingCreeper_update_eqnx = function(e)
            if self:isActive() then
				core.vfx.spawn("VFX_Summon_end", self.position)
                --core.sound.playSound3d("conjuration hit", self)

                anim.clearAnimationQueue(self, true)
                anim.playQueued(self, "knockout", {
                    loops = 0
                })
            end
            --[[             AI.startPackage({
                type = "Wander",
                distance = 4096,
                duration = 24,
                idle = {
                    idle2 = 60,
                    idle3 = 50,
                    idle4 = 40,
                    idle5 = 30,
                    idle6 = 20,
                    idle7 = 10,
                    idle8 = 0,
                    idle9 = 25
                },
                isRepeat = true,
                cancelOther = false
            }) ]]
        end
    }
}
