local CHANGES = [[
    Fixed api requirement
]]

return setmetatable({
    MOD_NAME = "Roaming Creeper",
    MOD_VERSION = 0.2,
    MIN_API = 60,
    CHANGES = CHANGES
}, {
    __tostring = function(modInfo)
        return string.format("\n[%s]\nVersion: %s\nMinimum API: %s\nChanges: %s", modInfo.MOD_NAME, modInfo.MOD_VERSION,
            modInfo.MIN_API, modInfo.CHANGES)
    end,
    __metatable = tostring
})

-- require("scripts.roaming_creeper.modInfo")
